package cu.yapapp.com.trimmer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.Toast;

import com.customview.trimmer.controller.TrimmerActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TestActivity extends AppCompatActivity {

    private static final String DIRECTORY_NAME = "Trimmer";
    private Button btnPickFile, btnPickFileSamsung;
    private final int requestCodePick = 1001;
    private final int requestCodeTrim = 1002;
    private Uri uri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPickFile = (Button) findViewById(R.id.pickFile);

        btnPickFile.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, requestCodePick);
            }
        });

        btnPickFileSamsung = (Button) findViewById(R.id.pickFileSamsung);

        btnPickFileSamsung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
                    intent.putExtra("CONTENT_TYPE", "*/*");
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    startActivityForResult(intent, requestCodePick);
                } catch (Exception e) {
                    Toast.makeText(TestActivity.this, "Device Does not look samsung", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK && requestCode == this.requestCodePick) {

            uri = data.getData();

            if (uri == null) return;

            String file_name = "";

            if (uri.toString().startsWith("content")) {

                new GetFileFromProviderAsync().execute();

            } else {
                file_name = uri.getPath();
                openTrimmer(file_name);
            }


        }

        if (resultCode == RESULT_OK && requestCode == this.requestCodeTrim) {

            String filePath = data.getStringExtra("uri");

            try {
                openFile(new File(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Toast.makeText(TestActivity.this, filePath, Toast.LENGTH_LONG).show();

        }


        if (resultCode == TrimmerActivity.RESULT_ERROR && requestCode == this.requestCodeTrim) {
            Toast.makeText(TestActivity.this, "Invalid File.", Toast.LENGTH_LONG).show();
        }

    }


    /**
     * this class does the operation to get file path .
     **/
    class GetFileFromProviderAsync extends AsyncTask<Void, Void, Void> {

        String file_name = "";
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(TestActivity.this);
            progressDialog.setMessage("Fetching file. Please wait...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            file_name = getFileFromProvider(uri).getAbsolutePath();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            if (file_name == null || file_name.length() == 0) {
                Toast.makeText(TestActivity.this, "Unable to get file ", Toast.LENGTH_LONG).show();
            } else
                openTrimmer(file_name);
        }
    }

    ;


    /**
     * Opens trimming activity
     * @param file_name : filepath  of file.
     */
    private void openTrimmer(String file_name) {
        Intent data = new Intent();
        data.putExtra("uri", file_name);
        data.setClass(this, TrimmerActivity.class);
        startActivityForResult(data, requestCodeTrim);
    }


    public void openFile(File url) throws IOException {
        // Create URI
        File file = url;
        uri = Uri.fromFile(file);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav");
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    /**
     *
     * This method tries to get file path of video from uri.
     * In case if file path is not present. It writes file  to a folder using input stream from URI and returns that path.
     *
     * @param uri : URI of video
     * @return : file path
     */
    public File getFileFromProvider(Uri uri) {


        String name = getFileNameFromProvider(uri);

        try {
            //  ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)
                    , DIRECTORY_NAME);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }

            mediaStorageDir = new File(mediaStorageDir.getAbsolutePath() + File.separator + name);
            mediaStorageDir.createNewFile();

            InputStream inputStream = getContentResolver().openInputStream(uri);

            OutputStream outputStream = new FileOutputStream(mediaStorageDir);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
            inputStream.close();
            outputStream.close();


            return mediaStorageDir;
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    /**
     * This method returns name of file whose URI is input.
     */
    public String getFileNameFromProvider(Uri imageUri) {
        Cursor returnCursor = getContentResolver().query(imageUri, null, null, null, null);
        //String mimeType = getContentResolver().getType(imageUri);
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = mime.getExtensionFromMimeType(getContentResolver().getType(imageUri));
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        if (!TextUtils.isEmpty(type) && !name.contains(type)) {
            name = name + "." + type;
        }
        return name;
    }
}
