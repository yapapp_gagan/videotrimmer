package com.customview.trimmer.model.controllerServiceManager;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;

/**
 * Created by gaganpreet on 5/9/17.
 */

public class TrimmerActivityManager {

    private TrimmerActivityManagerCallback managerCallback;
    private Context context;

    private final String TAG_LIFECYCLE = "FFMPEG_Lifecycle";
    private File outputFile;
    private FFmpeg ffmpeg;


    public static final int ERROR_FFMPEG_UNSUPPORTED = 100;
    public static final int ERROR_FAILURE_WHILE_TRIMMING = 102;
    public static final int ERROR_FFMPEG_RUNNING_EXCEPTION = 103;
    public static final int ERROR_FFMPEG_FAILURE_WHILE_LOADING = 104;


    public TrimmerActivityManager(Context context, TrimmerActivityManagerCallback managerCallback) {
        this.managerCallback = managerCallback;
        this.context = context;
        loadFFMpegBinary();
    }


    /**
     * Call this method when we want to trim any video file.
     * */
    public void trim(Uri uri, int startMs, int endMs) {

        try {
            String name = uri.getLastPathSegment();
            String outputFilePath = uri.getPath();
            String token_file_name = ".";
            String extension = outputFilePath.substring(outputFilePath.lastIndexOf(token_file_name));
            int lastIndex = name.lastIndexOf(extension);
            String name_without_extension = name.substring(0, lastIndex);
            String directory = uri.getPath();
            directory = directory.replace(uri.getLastPathSegment(), "");
            String newPath = directory + name_without_extension + "_1" + extension;
            outputFile = new File(newPath);
            if (outputFile.exists()) {
                outputFile.delete();
            }


            /****
             * Please do not remove the comments below
             //            execFFmpegBinary("-i " + uri.getPath()
             //                    + " -ss " + startMs / 1000 + " -to " + endMs / 1000 + " -strict -2 -async 1 "
             //                    + outputFile.getAbsolutePath());
             //            ffmpeg -i movie.mp4 -ss 00:00:03 -t 00:00:08 -async 1 -c copy cut.mp4
             **/


            execFFmpegBinary("-i " + uri.getPath()
                    + " -ss " + startMs / 1000 + " -to " + endMs / 1000 + " -async 1 -c copy "
                    + outputFile.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * FFMPEG command is executed in this method.
     * The progress/success/failures are send back through {@link TrimmerActivityManagerCallback} instance.
     * */
    private void execFFmpegBinary(final String command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.e(TAG_LIFECYCLE, "FAILED with output : " + s);
                    managerCallback.onError(ERROR_FAILURE_WHILE_TRIMMING, null, s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.e(TAG_LIFECYCLE, "onSuccess : " + s);
                    managerCallback.onSuccessfullySaved(outputFile.getPath());
                }

                @Override
                public void onProgress(String s) {
                    Log.e(TAG_LIFECYCLE, "onProgress : " + s);
                    managerCallback.onProgress(s);
                }

                @Override
                public void onStart() {
                    Log.e(TAG_LIFECYCLE, "onStart ");
                    managerCallback.start();
                }

                @Override
                public void onFinish() {
                    Log.e(TAG_LIFECYCLE, "onFinish");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
            managerCallback.onError(ERROR_FFMPEG_RUNNING_EXCEPTION, e, e.getMessage());
        }
    }


    /**
     * This method create instance of FFMPEG and sends back error in case the device not supported.
     **/
    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                ffmpeg = FFmpeg.getInstance(context);
            }

            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    managerCallback.onError(ERROR_FFMPEG_FAILURE_WHILE_LOADING, null, null);
                }

                @Override
                public void onSuccess() {
                    /**
                     * We can perform operations when success comes.
                     * */
                }
            });
        } catch (FFmpegNotSupportedException e) {
            managerCallback.onError(ERROR_FFMPEG_UNSUPPORTED, e, e.getMessage());
        } catch (Exception e) {
            managerCallback.onError(ERROR_FFMPEG_FAILURE_WHILE_LOADING, e, e.getLocalizedMessage());
        }
    }


    public interface TrimmerActivityManagerCallback {
        public void onSuccessfullySaved(String filePath);

        public void start();

        public void onProgress(String s);

        public void onError(int errorCode, Exception e, String comment);
    }


}
