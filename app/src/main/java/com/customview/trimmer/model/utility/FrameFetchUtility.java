package com.customview.trimmer.model.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.concurrent.TimeUnit;

import cu.yapapp.com.trimmer.R;

/**
 * Created by gaganpreet on 4/9/17.
 */

public class FrameFetchUtility {


    private Context mContext;
    private LinearLayout galleryContainer;
    private Uri uri;
    private long duration;
    private Bitmap[] frames;
    private FramesGetter framesGetter = null;
    private int windowWidth;
    private FrameFetchCallback frameFetchCallback;


    /**
     *
     * @param timeInMills Input is in millis.
     * @return the time in hh:mm:ss
     */
    public String getTimeForTrackFormat(int timeInMills) {
        String s = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(timeInMills),
                TimeUnit.MILLISECONDS.toMinutes(timeInMills) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeInMills)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(timeInMills) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeInMills)));
        return s;

    }

    /**
     *
     * @param frameCallback input is {@link FrameFetchCallback} instance which gives callback to the caller of this method.
     */
    public void setFrameCallback(FrameFetchCallback frameCallback) {
        this.frameFetchCallback = frameCallback;
    }


    /**
     *  This class handles callback for {@link FrameFetchUtility}
     */
    public interface FrameFetchCallback {
        public void onCompleted(long[] durations, Bitmap[] bitmaps);
    }


    /**
     *
     * @param mContext : Context of caller
     * @param uri : Valid Video Uri whose path must be available.
     * @param host : Container for images
     * @param widthOfHost : width of container
     */
    public void fetchImageCaptureOperation(Context mContext, Uri uri, LinearLayout host, int widthOfHost) {

        this.mContext = mContext;
        this.uri = uri;
        this.galleryContainer = host;
        this.windowWidth = widthOfHost;


        if (framesGetter != null) {
            framesGetter.cancel(true);
        }
        framesGetter = new FramesGetter();
        framesGetter.execute();
    }


    class FramesGetter extends AsyncTask<Void, Void, Void> {
        private final Context context;
        private MediaMetadataRetriever mediaMetadataRetriever;
        private float heightOfImageReqd;
        private float widthOfImageReqd;
        private long timeframes[];

        FramesGetter() {
            context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            heightOfImageReqd = context.getResources().getDimension(R.dimen.heightOfGallery);
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(context, uri);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            duration = Long.parseLong(time);
            Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(0); //unit in microsecond
            float videoWidth = bmFrame.getWidth();
            float videoHeight = bmFrame.getHeight();
            float ratioImageWidthToHeight = videoWidth / videoHeight;
            widthOfImageReqd = heightOfImageReqd * ratioImageWidthToHeight;
//            int count = Math.round(windowWidth / widthOfImageReqd); // dont remove this line. It can be used in future.
            int count = Math.round(windowWidth / heightOfImageReqd);// divided by height to get more number of frames
            timeframes = getTimeFramesBasedOnLength(duration, count);
            frames = getFramesBasedUponTimeFrame(timeframes);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if (galleryContainer.getChildCount() > 0)
                galleryContainer.removeAllViews();
            for (int i = 0; i < frames.length; i++) {
                ImageView imageView = new ImageView(context);
                imageView.setLayoutParams(new RelativeLayout.LayoutParams((int) heightOfImageReqd, (int) heightOfImageReqd));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setImageBitmap(frames[i]);
                galleryContainer.addView(imageView);
            }

            if (frameFetchCallback != null)
                frameFetchCallback.onCompleted(timeframes, frames);

        }

        /**
         *
         * @param timeframes : Time array in millis
         * @return Bitmap from a video at the time indexes.
         */
        private Bitmap[] getFramesBasedUponTimeFrame(long[] timeframes) {
            /**
             * Please dont remove the comments. these are here in case we need to check bugs in galleryContainer.
             //            String path = Environment.getExternalStorageDirectory() + File.separator + "Trimmer" + File.separator + System.currentTimeMillis();
             //            File folder = new File(path);
             //            folder.mkdirs();
             */

            Bitmap[] frameObjects = new Bitmap[timeframes.length];

            for (int i = 0; i < frameObjects.length; i++) {
                long duration_fetched_micros = timeframes[i] * 1000;

                /**
                 * Get frame using android's media retriever
                 * */
                Bitmap bitmap = getFrameAtTime(duration_fetched_micros);
                frameObjects[i] = Bitmap.createScaledBitmap(bitmap, (int) widthOfImageReqd, (int) heightOfImageReqd, false);


                /**
                 *          Please dont remove the comments. these are here in case we need to check bugs in galleryContainer.
                 //                File file = new File(folder.getAbsolutePath() + File.separator + getTimeForTrackFormat((int) timeframes[i]) + ".png");
                 //                try {
                 //                    frames[i].compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
                 //                } catch (FileNotFoundException e) {
                 //                    e.printStackTrace();
                 //                }
                 * */

            }
            mediaMetadataRetriever.release();

            return frameObjects;
        }

        /**
         *
         * @param duration_fetched_micros Time in micros
         * @return Bitmap in the video
         */
        private Bitmap getFrameAtTime(long duration_fetched_micros) {
            Bitmap bitmap = framesGetter.mediaMetadataRetriever.getFrameAtTime(duration_fetched_micros);
            return bitmap;
        }

        /**
         * This method does the calculation work for returning time frames.
         *
         * @param duration  : Time of video
         * @param count : count of images required.
         * @return : timeframes array
         */
        private long[] getTimeFramesBasedOnLength(long duration, int count) {
            long[] timeFrames = new long[count];
            long single = (duration / count);
            Log.e("tag_frame_fetch single", getTimeForTrackFormat((int) single) + "");
            for (int i = 0; i < count; i++) {
                timeFrames[i] = single * (i /*+ 1*/);
                Log.e("tag_frame_fetch frames", getTimeForTrackFormat((int) timeFrames[i]) + "");
            }
            return timeFrames;
        }


    }


    /**
     *
     * @param context Context
     * @param uri Uri
     * @return true if file is valid or not.
     */
    public boolean isValidFile(Context context, Uri uri) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setDataSource(context, uri);
        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }


}
