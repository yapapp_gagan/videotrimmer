package com.customview.trimmer.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.customview.trimmer.model.utility.FrameFetchUtility;

import net.protyposis.android.mediaplayer.MediaPlayer;
import net.protyposis.android.mediaplayer.VideoView;
import net.protyposis.android.mediaplayer.MediaPlayer.OnPreparedListener;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.io.File;
import java.util.concurrent.TimeUnit;

import cu.yapapp.com.trimmer.R;

/**
 * Created by gaganpreet on 5/9/17.
 */
public class TrimmerView extends RelativeLayout {


    private View parent;

    private TextView tvStartTime;
    private TextView tvProgressTime;
    private TextView tvEndTime;
    private TextView tvMemoryInfo;

    private RangeSeekBar<Integer> rsbSeek;
    private RangeSeekBar<Integer> rsbPointer;

    private ImageView ivPlayPause;
    private ImageView ivSave;
    private ImageView ivStop;
    private ImageView ivSkip;

    private VideoView videoView;

    private ProgressDialog pdProgress;

    private FrameFetchUtility frameFetchUtility = null;

    private LinearLayout galleryContainer;

    private Uri uri;
    private boolean isPreparedFirstTime = false;

    private boolean isStopped = true;
    private TrimmerViewListener trimmerViewListener;
    private int oldSelectedMinValue = 0;
    private int oldSelectedMaxValue = 0;


    private final float SIZE_OF_BYTES_IN_KB = 1024f;
    private final float ALLOWED_SIZE_IN_BYTES = 20 * SIZE_OF_BYTES_IN_KB * SIZE_OF_BYTES_IN_KB;

    private int totalDuration = 0;
    private long memoryInBytes = 0;
    private int allowedDurationDifference = 0;

    private Handler handler = new Handler();
    private final int REOCCURANCE_TIME_FOR_SEEKER = 50;

    public TrimmerView(Context context) {
        super(context);
    }


    public void showDialog(String message) {
        if (!pdProgress.isShowing()) {
            pdProgress.setCancelable(false);
            pdProgress.setMessage(message);
            pdProgress.show();
        }
    }

    public void dismissDialog() {
        if (pdProgress.isShowing())
            pdProgress.dismiss();
    }


    public TrimmerView(Context context, Uri uri, FrameFetchUtility frameFetchUtility, TrimmerViewListener trimmerViewListener) {
        super(context);
        this.uri = uri;
        this.frameFetchUtility = frameFetchUtility;
        this.trimmerViewListener = trimmerViewListener;
        onInit();
    }


    public TrimmerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TrimmerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TrimmerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Initialisation of views and event handling is performed in this method.
     */
    private void onInit() {

        parent = inflate(getContext(), R.layout.activity_video_view, null);
        rsbSeek = (RangeSeekBar<Integer>) parent.findViewById(R.id.rsbSeekbar);
        rsbPointer = (RangeSeekBar<Integer>) parent.findViewById(R.id.rsbPointer);
        ivPlayPause = (ImageView) parent.findViewById(R.id.ivPlausePlay);
        tvStartTime = (TextView) parent.findViewById(R.id.tvStartText);
        tvProgressTime = (TextView) parent.findViewById(R.id.tvProgressTime);
        tvEndTime = (TextView) parent.findViewById(R.id.tvEndTime);
        tvMemoryInfo = (TextView) parent.findViewById(R.id.tvMemoryInfo);
        ivSave = (ImageView) parent.findViewById(R.id.ivSave);
        ivStop = (ImageView) parent.findViewById(R.id.ivStop);
        ivSkip = (ImageView) parent.findViewById(R.id.ivSkip);
        videoView = (VideoView) parent.findViewById(R.id.vvVideo);
        galleryContainer = (LinearLayout) parent.findViewById(R.id.llGalleryContainer);
        pdProgress = new ProgressDialog(getContext());

        videoView.setVideoURI(uri);
        videoView.setOnPreparedListener(onPreparedListener);
        ivPlayPause.setOnClickListener(playPauseClick);
        ivSave.setOnClickListener(onSaveClick);
        ivStop.setOnClickListener(onStopClick);
        ivSkip.setOnClickListener(onSkipClick);
        frameFetchUtility.setFrameCallback(frameFetchCallback);

        addView(parent);

        showDialog(getResources().getString(R.string.please_wait_loading_frames));

        rsbPointer.setClickable(false);
        rsbPointer.setFocusable(false);

    }


    FrameFetchUtility.FrameFetchCallback frameFetchCallback = new FrameFetchUtility.FrameFetchCallback() {
        @Override
        public void onCompleted(long[] durations, Bitmap[] bitmaps) {
            dismissDialog();
            trimmerViewListener.onFramesLoaded();
        }
    };

    OnClickListener onSkipClick = new OnClickListener() {
        @Override
        public void onClick(View view) {
            trimmerViewListener.skipTrimmer();
        }
    };

    OnPreparedListener onPreparedListener = new OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {

            if (isPreparedFirstTime)
                return;

            totalDuration = mediaPlayer.getDuration();
            memoryInBytes = getFileSize(uri.getPath());


            tvEndTime.setText(getTimeForTrackFormat(totalDuration));
            tvStartTime.setText(getTimeForTrackFormat(0));


            rsbSeek.setRangeValues(0, totalDuration);
            rsbPointer.setRangeValues(0, totalDuration);

            rsbSeek.setOnRangeSeekBarChangeListener(rangeSelector);


            if (memoryInBytes < ALLOWED_SIZE_IN_BYTES) {
                allowedDurationDifference = totalDuration;
                showMemoryInformation(0, totalDuration);
                oldSelectedMaxValue = totalDuration;
            } else {
                allowedDurationDifference = calculateAllowedDurationDifference(totalDuration, memoryInBytes, (int) ALLOWED_SIZE_IN_BYTES);
                rsbSeek.setSelectedMaxValue(allowedDurationDifference);
                tvEndTime.setText(getTimeForTrackFormat(allowedDurationDifference));
                showMemoryInformation(0, allowedDurationDifference);
                oldSelectedMaxValue = allowedDurationDifference;
            }


            isPreparedFirstTime = true;

            videoView.seekTo(0);

            frameFetchUtility.fetchImageCaptureOperation(getContext(), uri, galleryContainer, galleryContainer.getWidth());

        }
    };

    /**
     * This method shows memory by the calculation below.
     * <br/>ratio = ((endDuration - startDuration ) totalDuration )* memory
     * <br/> float bytes = ratio * memoryInBytes<its total memory size of video>
     *
     * @param startDuration : starting duration of seekbar
     * @param endDuration   : end duration of seekbar
     */
    private void showMemoryInformation(int startDuration, int endDuration) {

//        0 , 5000
//                5000
//        (5000-0)/5000 * 4.4 = 4.4
//         ((endDuration - startDuration ) totalDuration )* memory

        int timeAfterTrimming = endDuration - startDuration;
        float ratio = (timeAfterTrimming * 1.0f) / (totalDuration * 1.0f);
        float _result_in_bytes = ratio * memoryInBytes;
        if (_result_in_bytes > SIZE_OF_BYTES_IN_KB) {
            float _result_in_kilo_bytes = _result_in_bytes / SIZE_OF_BYTES_IN_KB;
            if (_result_in_kilo_bytes > SIZE_OF_BYTES_IN_KB) {
                float _result_in_mb_bytes = _result_in_kilo_bytes / SIZE_OF_BYTES_IN_KB;
                if (_result_in_mb_bytes > SIZE_OF_BYTES_IN_KB) {
                    float _result_in_gb_bytes = _result_in_mb_bytes / SIZE_OF_BYTES_IN_KB;
                    tvMemoryInfo.setText(round(_result_in_gb_bytes) + " gb");
                } else {
                    tvMemoryInfo.setText(round(_result_in_mb_bytes) + " mb");
                }
            } else {
                tvMemoryInfo.setText(round(_result_in_kilo_bytes) + " kb");
            }
        } else {
            tvMemoryInfo.setText(round(_result_in_bytes) + " bytes");
        }


    }

    /**
     * @param value input value
     * @return roundoff string of input up to 2 decimals.
     */
    private String round(float value) {
        double roundOff = (double) Math.round(value * 100) / 100;

        String data = "";

        if (roundOff == Math.round(roundOff)) data = String.valueOf(Math.round(roundOff));
        else data = String.valueOf(roundOff);

        return data;
    }


    /**
     * get file size
     * <ul>
     * <li>if path is null or empty, return -1</li>
     * <li>if path exist and it is a file, return file size, else return -1</li>
     * <ul>
     *
     * @param path
     * @return returns the length of this file in bytes. returns -1 if the file does not exist.
     */
    public static long getFileSize(String path) {
        if (TextUtils.isEmpty(path)) {
            return -1;
        }
        File file = new File(path);
        return (file.exists() && file.isFile() ? file.length() : -1);
    }


    /**
     * Method for handling "Error in trimming" dialog
     */
    public void showUnsupportedExceptionDialog() {
        showErrorDialog(android.R.drawable.ic_dialog_alert
                , getContext().getString(R.string.device_not_supported_message), false);
    }


    /**
     * This method is used to show error dialogs in trimmer.
     * This dialog shows OK button on click of it callback is sent to the caller.
     *
     * @param icon          : Icon resource
     * @param message       : Message to display
     * @param isCancellable : Dialog is cancellable or not.
     */
    private void showErrorDialog(int icon, String message, boolean isCancellable) {
        new AlertDialog.Builder(getContext())
                .setIcon(icon)
                .setMessage(message)
                .setCancelable(isCancellable)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        trimmerViewListener.onError();
                    }
                })
                .create()
                .show();
    }


    /**
     * Method for handling "Error in trimming" dialog
     */
    public void showErrorWhileTrimmingDialog() {
        showErrorDialog(android.R.drawable.ic_dialog_alert
                , getContext().getString(R.string.error_trimming), false);
    }


    /**
     * @param duration    : total time of video
     * @param totalSize   : total memory size of video
     * @param allowedSize : allowed memory size of trimmed video
     * @return : allowed duration difference of video.
     */
    private int calculateAllowedDurationDifference(int duration, long totalSize, int allowedSize) {

//        5000ms  - 4mb
//        if allowed is 2mb
//        (5000ms / 4) * 2 - 2mb
//        1500 - 2mb
//        so allowed is 1500ms difference

        float allowed = ((duration * 1f) / (totalSize * 1f)) * (allowedSize * 1f);
        return Math.round(allowed);
    }


    RangeSeekBar.OnRangeSeekBarChangeListener<Integer> rangeSelector = new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
        @Override
        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer _minValue, Integer _maxValue) {

            int minValueInt = _minValue.intValue();
            int maxValueInt = _maxValue.intValue();

            boolean isMinValChanged = oldSelectedMinValue != minValueInt;
            boolean isMaxValChanged = oldSelectedMaxValue != maxValueInt;

            final int durationDifference = maxValueInt - minValueInt;

            final boolean isManualChangeRequired = durationDifference > allowedDurationDifference;

            if (isManualChangeRequired) {

                final int changer = durationDifference - allowedDurationDifference;

                rsbSeek.setOnRangeSeekBarChangeListener(null);

                if (isMinValChanged) {
                    maxValueInt = maxValueInt - changer;
                    isMaxValChanged = true;
                    rsbSeek.setSelectedMaxValue(maxValueInt);
                } else {
                    minValueInt = minValueInt + changer;
                    isMinValChanged = true;
                    rsbSeek.setSelectedMinValue(minValueInt);
                }

                rsbSeek.setOnRangeSeekBarChangeListener(this);

            }


            showMemoryInformation(minValueInt, maxValueInt);


            boolean isPlaying = videoView.isPlaying();

            if (isPlaying) {

                if (isMinValChanged) {
                    ivStop.performClick();
                    videoView.seekTo(minValueInt);
                }
            }

            if (isMinValChanged) {
                tvStartTime.setText(getTimeForTrackFormat(minValueInt));
                rsbPointer.setSelectedMinValue(minValueInt);
                rsbPointer.setSelectedMaxValue(minValueInt);
                videoView.seekTo(minValueInt);
            }

            if (isMaxValChanged) {
                tvEndTime.setText(getTimeForTrackFormat(maxValueInt));
            }

            oldSelectedMinValue = minValueInt;
            oldSelectedMaxValue = maxValueInt;
        }
    };

    View.OnClickListener onSaveClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ivStop.performClick();
            Log.e("tag_trim", getTimeForTrackFormat(rsbSeek.getSelectedMinValue()) + " -> " + getTimeForTrackFormat(rsbSeek.getSelectedMaxValue()));
            trimmerViewListener.trim(rsbSeek.getSelectedMinValue(), rsbSeek.getSelectedMaxValue());
        }
    };

    View.OnClickListener onStopClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setStoppedStatus(true);

            handler.removeCallbacks(runnable);
            videoView.stopPlayback();
            videoView.setOnPreparedListener(onPreparedListener);
            videoView.setVideoURI(uri);

            rsbPointer.setVisibility(View.GONE);

            videoView.seekTo(rsbSeek.getSelectedMinValue());
        }
    };


    View.OnClickListener playPauseClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            isStopped = !isStopped;
            if (isStopped) {
                taskPause();
            } else {
                taskStart();
            }
        }
    };


    /**
     * When video is paused this method is called.
     */
    private void taskPause() {
        videoView.pause();
        rsbPointer.setVisibility(View.GONE);
        handler.removeCallbacks(runnable);
        setStoppedStatus(isStopped);
    }


    /**
     * When video is played this method is called.
     */
    private void taskStart() {
        int min = rsbSeek.getSelectedMinValue();
        rsbPointer.setSelectedMinValue(min);
        rsbPointer.setSelectedMaxValue(min);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                videoView.start();
                rsbPointer.setVisibility(View.VISIBLE);
                handler.postDelayed(runnable, REOCCURANCE_TIME_FOR_SEEKER);
                setStoppedStatus(isStopped);
            }
        }, 300);
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            int position = videoView.getCurrentPosition();

            if ((position < rsbSeek.getSelectedMaxValue() - REOCCURANCE_TIME_FOR_SEEKER)) {

                rsbPointer.setSelectedMinValue(position);
                rsbPointer.setSelectedMaxValue(position);

                tvProgressTime.setText(getTimeForTrackFormat(position));
                handler.postDelayed(runnable, 50);
            } else {
                ivStop.performClick();
                Log.e("stopped", "stopped");
            }

        }
    };


    /**
     * @param timeInMills Input is in millis.
     * @return the time in hh:mm:ss
     */
    public static String getTimeForTrackFormat(int timeInMills) {
        String s = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(timeInMills),
                TimeUnit.MILLISECONDS.toMinutes(timeInMills) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeInMills)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(timeInMills) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeInMills)));
        return s;
    }


    /**
     * It performs UI operations when video is stopped/ played.
     *
     * @param b if the video is stopped
     */
    private void setStoppedStatus(boolean b) {
        isStopped = b;
        if (isStopped) {
            tvProgressTime.setVisibility(View.GONE);
            ivPlayPause.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
        } else {
            tvProgressTime.setText("");
            tvProgressTime.setVisibility(View.VISIBLE);
            ivPlayPause.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
        }
    }

    public void showMessage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_LONG).show();
    }


    public interface TrimmerViewListener {
        public void onError();

        void trim(int selectedMinValue, int selectedMaxValue);

        public void skipTrimmer();

        public void onFramesLoaded();

    }


}
