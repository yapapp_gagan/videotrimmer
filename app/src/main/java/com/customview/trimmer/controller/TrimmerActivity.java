package com.customview.trimmer.controller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.customview.trimmer.model.controllerServiceManager.TrimmerActivityManager;
import com.customview.trimmer.model.utility.FrameFetchUtility;
import com.customview.trimmer.view.TrimmerView;

import java.io.File;

import cu.yapapp.com.trimmer.R;

public class TrimmerActivity extends Activity {

    public static final int RESULT_ERROR = 10000;
    private TrimmerActivityManager manager;
    private FrameFetchUtility frameFetchUtility;
    private TrimmerView trimmerView;
    private String filePath;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        filePath = getIntent().getStringExtra("uri");
        uri = Uri.fromFile(new File(filePath));
        frameFetchUtility = new FrameFetchUtility();


        manager = new TrimmerActivityManager(TrimmerActivity.this, trimmerManagerCallback);
        trimmerView = new TrimmerView(this, uri, frameFetchUtility, trimmerViewCallback);
        setContentView(trimmerView);



        if (!frameFetchUtility.isValidFile(this, uri)) {
            onError();
        }


    }


    TrimmerView.TrimmerViewListener trimmerViewCallback = new TrimmerView.TrimmerViewListener() {
        @Override
        public void onError() {
            TrimmerActivity.this.onError();
        }

        @Override
        public void trim(int selectedMinValue, int selectedMaxValue) {
            manager.trim(uri, selectedMinValue, selectedMaxValue);
        }

        @Override
        public void skipTrimmer() {
            returnFilePath(filePath);
        }

        @Override
        public void onFramesLoaded() {
            /**
             * Callback to when gallery is loaded
             * **/
        }
    };


    TrimmerActivityManager.TrimmerActivityManagerCallback trimmerManagerCallback = new TrimmerActivityManager.TrimmerActivityManagerCallback() {
        @Override
        public void onSuccessfullySaved(String filePath) {
            trimmerView.dismissDialog();
            returnFilePath(filePath);
        }


        @Override
        public void start() {
            trimmerView.showDialog(getString(R.string.trimming_started));
        }

        @Override
        public void onProgress(String log) {
            /**
             * This is log output of FFMPEG
             * */
        }

        @Override
        public void onError(int errorCode, Exception e, String comment) {
            switch (errorCode) {
                case TrimmerActivityManager.ERROR_FFMPEG_FAILURE_WHILE_LOADING:
                    trimmerView.showUnsupportedExceptionDialog();
                    break;

                case TrimmerActivityManager.ERROR_FAILURE_WHILE_TRIMMING:
                    trimmerView.dismissDialog();
                    trimmerView.showErrorWhileTrimmingDialog();
                    break;

                case TrimmerActivityManager.ERROR_FFMPEG_UNSUPPORTED:
                    trimmerView.showUnsupportedExceptionDialog();
                    break;


                case TrimmerActivityManager.ERROR_FFMPEG_RUNNING_EXCEPTION:
                default:
                    trimmerView.showMessage(getString(R.string.error_while_trimming));
                    trimmerView.dismissDialog();
                    trimmerView.showErrorWhileTrimmingDialog();
                    break;
            }
        }
    };

    /**
     *
     * This sends back path of trimmed video.
     *
     * @param filePath : file path of trimmed video
     */
    private void returnFilePath(String filePath) {
        Intent intent = new Intent();
        intent.putExtra("uri", filePath);
        setResult(RESULT_OK, intent);
        finish();
    }


    /***
     * This method sends error activity.
     */
    public void onError() {
        setResult(RESULT_ERROR);
        finish();
    }


}
