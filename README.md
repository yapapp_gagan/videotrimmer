# Video Trimmer's README

This utility can be used for trimming videos.

# Dependencies Used:
1. FFmpegAndroid-0.2.5.aar :-
The main trimming operation is done using FFMPEG.

2. rangeseekbar-library:0.3.0:
Used for seeking purposes(org.florescu.android.rangeseekbar:rangeseekbar-library:0.3.0).

3. net.protyposis.android.mediaplayer:mediaplayer:4.3.0 :-
It is used for rendering of video.

4. net.protyposis.android.mediaplayer:mediaplayer-dash:4.3.0 :-
It is used for rendering of video.


# Configuration:
1. Minimum SDK Ver.:     16 (We can't use less than 16 because one of the dependency is using 16 version.)
2. Target SDK Ver.: 20
3. compileSdkVersion: 26
4. buildToolsVersion: 26.0.1


# Classes Used:

1. TrimmerActivity:
It does all the work. The calling activity needs to start it(using startActivityForResult)
once trimming is done the TrimmerActivity sends trimmed video file path in intent using sendResult.
The snippet below shows the work after trimming.
```java
    /**  
     *
     * This sends back path of trimmed video.
     *
     * @param filePath : file path of trimmed video
     **/
    private void returnFilePath(String filePath) {
        Intent intent = new Intent();
        intent.putExtra("uri", filePath);
        setResult(RESULT_OK, intent);
        finish();
    }
```
    
2. TrimmerView:
It has all UI controls like RangeSeekbar, TextViews and buttons(like trim , skip , play/pause and stop).
This class internally uses FrameFetchUtility for fetching frames and adding frames in linearlayout container.
When user selects desired range of video to be trimmed, it sends callback to TrimmerActivity which then use TrimmerActivityManager for trimming purposes.

3. TrimmerActivityManager:
This class is responsible for initialisation of  FFMPEG, trimming and sending filepath to TrimmerActivity, which then sends path back to calling activity.

4. FrameFetchUtility:
The function of this class is to fetch frames from video at calculated timeframes and to add in LinearLayout of TrimmerView.


# How to use
We needs to get valid file path of video sends to TrimmerActivity. Please see the method of TestActivity below for better understanding.

```java
    /**
     * Opens trimming activity
     * @param file_name : filepath  of file.
     */
    private void openTrimmer(String file_name) {
        Intent data = new Intent();
        data.putExtra("uri", file_name);
        data.setClass(this, TrimmerActivity.class);
        startActivityForResult(data, requestCodeTrim);
    }

```


To get the result from TrimmerActivity we need to get String 'uri' from intent. Please see the sample code for this also.

```java
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == this.requestCodeTrim) {
            String filePath = data.getStringExtra("uri");
            try {
                openFile(new File(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(TestActivity.this, filePath, Toast.LENGTH_LONG).show();
        }
    }

```

# Known Limitation:

The trimmer trim the Uri's whose file paths exists on the system. It doesn't process input streams for now.